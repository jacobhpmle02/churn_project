# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 09:33:27 2021

@author: OscarKelly
"""

import pandas as pd
import numpy as np

from lightgbm import LGBMClassifier
from sklearn.metrics import accuracy_score, roc_auc_score, f1_score, plot_roc_curve, precision_recall_curve, confusion_matrix


# %%
# Training

train = pd.read_csv('banking_churn_data/train_final.csv')

train = train.set_index('date')


#optional dropping
train.drop(['num_deposit','month_start_bal','GDP'], axis=1, inplace=True)


features = [col for col in train.columns if col not in ['customer_id','churn']]
target='churn'
X_train = train[features]
X_train = pd.get_dummies(X_train)
redundant_regions= ['Region_East North Central',
       'Region_East South Central', 'Region_Middle Atlantic',
       'Region_Mountain', 'Region_New England',
       'Region_South Atlantic', 'Region_West North Central']
X_train.drop(redundant_regions, axis=1, inplace=True)
y_train = train[target]

model= LGBMClassifier(subsample_for_bin=2000000,
                      n_estimators=244,
                      max_depth=11,
                      learning_rate=0.038657198003864156,
                      feature_fraction=1.0)

model.fit(X_train, y_train)

# %%
# Inference

test = pd.read_csv('banking_churn_data/test_final.csv')
test = test.set_index('date')

test.drop(['num_deposit','month_start_bal','GDP'], axis=1, inplace=True)
features = [col for col in test.columns if col not in ['customer_id','churn']]

# getting account_id
trans = pd.read_csv('banking_churn_data/transactions_tm1_e.csv')
trans['date']=pd.to_datetime(trans['date'])
trans = trans[trans['date']>=pd.to_datetime('2020-05-31')]
trans = trans[['customer_id', 'account_id']]
trans = trans.drop_duplicates()

test = test.merge(trans, how='left', on='customer_id')

X_test = test[features]
X_test = pd.get_dummies(X_test)
redundant_regions= ['Region_East North Central',
       'Region_East South Central', 'Region_Middle Atlantic',
       'Region_Mountain', 'Region_New England',
       'Region_South Atlantic', 'Region_West North Central']
X_test.drop(redundant_regions, axis=1, inplace=True)


test['prob_churn'] = model.predict_proba(X_test)[:,1]


sub =  pd.read_csv('banking_churn_data/submission_sample.csv')
test = test[['account_id', 'prob_churn']]
sub=sub.merge(test, on='account_id', how='left')
sub.drop('pred_churn', axis=1, inplace=True)
sub.columns=['account_id', 'date', 'pred_churn']
sub.to_csv('banking_churn_data/submission_dionysos.csv', index=False)


# %%

'''
y_hat = model.predict_proba(X_val)[:,1]
y_pred = model.predict(X_val)
#y_pred = (model.predict_proba(X_val)[:,1]>0.4).astype(int)
print('AUC VAL', roc_auc_score(y_val, y_hat))
print('F1_val', f1_score(y_val, y_pred))

#roc curve plot
plot_roc_curve(model, X_val, y_val)


#precision recall curve plot
prec, rec,_ = precision_recall_curve(y_val, y_hat)
fig, ax =plt.subplots()
ax.plot(rec, prec)
ax.set_xlabel('Recall')
ax.set_ylabel('Precision')

#confusion matrix
C  = confusion_matrix(y_val, y_pred)
print('True negatives:', C[0,0])
print('False Negatives:',C[1,0])
print('True Positives:', C[1,1])
print('False Positives:', C[0,1])


from lightgbm import plot_importance
plot_importance(model)
'''