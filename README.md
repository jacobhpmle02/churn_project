# Team Dionysos MLE02 Banking Churn Project
----
Olivia Brooker, Milan Dave, Jacob Howard-Parker, Oscar Kelly, Ollie Williams

This repository contains code for our banking churn project.


**Key files:**

* data_integrity.py - performs basic cleaning (available with commentary in eda_and_notebooks/data_integrity.ipynb)

* feature_engineering.py - data aggregation and feature engineering (also available as notebook in eda_and_notebooks/data_aggregation.ipynb)

* mlflow_experiment_lgbm.py - file for mlflow experiment using lightgbm

* model_evaluation.py - file for evaluating chosen model on validation and test data

* lightgbm_inference.py - file for full training and inference on submission.csv

**Additional files:**

* EDA_internal_data.ipynb - rough code for exploratory data analysis


