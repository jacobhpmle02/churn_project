'''
Author: Milan Dave
Date  : 14th April 2021

Overview
--------

In this script we will be writing the tracking functions for our churn project. 
Error metrics and all that.

'''
#%%         Tracking URI

tracking_uri = 'http://52.90.101.28:5000'


#%%         Imports

import pandas as pd
import numpy as np
import argparse
import mlflow

from lightgbm import LGBMClassifier

from sklearn.metrics import accuracy_score, roc_auc_score, f1_score, confusion_matrix
from sklearn.model_selection import train_test_split

#%% Train val split

train = pd.read_csv('banking_churn_data/train_final.csv')
train = train.set_index('date')

#optional dropping
train.drop(['num_deposit','month_start_bal','GDP'], axis=1, inplace=True)

features = [col for col in train.columns if col not in ['customer_id','churn']]
target='churn'

X_train = train[features]
X_train = pd.get_dummies(X_train)
redundant_regions= ['Region_East North Central',
       'Region_East South Central', 'Region_Middle Atlantic',
       'Region_Mountain', 'Region_New England',
       'Region_South Atlantic', 'Region_West North Central']
X_train.drop(redundant_regions, axis=1, inplace=True)
y_train = train[target]

X_trn, X_val, y_trn, y_val = train_test_split(X_train, y_train, shuffle=False, test_size=0.1)


#%%         Hyperparameters

parser = argparse.ArgumentParser('Dionysos_lightgbm_2014')
parser.add_argument('--n', type=int, default=50, help='Number of experiments')

args, _ = parser.parse_known_args()
n_runs = args.n

# %%        Models

mlflow.set_tracking_uri(tracking_uri)
mlflow.set_experiment('Dionysos_lightgbm')

for i in range(n_runs):
    with mlflow.start_run():

        # Hyperparameters
        learning_rate = np.random.random()*0.3
        max_depth     = np.random.randint(10,32)
        n_estimators  = np.random.randint(64,256)
        feature_fraction = np.random.choice([0.8, 0.9, 1])

        # Creating and fitting model
        model = LGBMClassifier(subsample_for_bin=2000000,n_estimators=n_estimators, max_depth=max_depth, 
                               learning_rate=learning_rate, feature_fraction=feature_fraction ,seed=123)

    
        model.fit(X_trn, y_trn)

        y_fpred_prob = model.predict_proba(X_val)[:,1]
        y_fpred = model.predict(X_val)

        accuracy = accuracy_score(y_val, y_fpred)
        roc_auc = roc_auc_score(y_val, y_fpred_prob)
        f1 = f1_score(y_val, y_fpred)
        C  = confusion_matrix(y_val, y_fpred)
        
        print(f'ROC_AUC score {roc_auc}')
        print(f'ACC {np.mean(accuracy)}')
        print(f'F1_score {f1}')
        
        print('True negatives:', C[0,0])
        print('False Negatives:',C[1,0])
        print('True Positives:', C[1,1])
        print('False Positives:', C[0,1])

        # Log metrics, params and model to mlflow.
        mlflow.log_metric('acc', accuracy)
        mlflow.log_metric('roc_auc', roc_auc)
        mlflow.log_metric('f1_score', f1)
        mlflow.log_metric('TN', C[0,0])
        mlflow.log_metric('FN', C[1,0])
        mlflow.log_metric('TP', C[1,1])
        mlflow.log_metric('FP', C[0,1])
        mlflow.log_param('learning_rate', learning_rate)
        mlflow.log_param('max_depth', max_depth)
        mlflow.log_param('n_estimators', n_estimators)
        mlflow.log_param('feature_fraction', feature_fraction)