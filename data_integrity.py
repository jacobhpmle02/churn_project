'''
Consolidates the cleaning from the data_integrity notebook to a simple
script
'''
#%%
# Imports and data read

import pandas as pd
import numpy as np

cust  = pd.read_csv('banking_churn_data/customers_tm1_e.csv')
trans = pd.read_csv('banking_churn_data/transactions_tm1_e.csv')

#%%
# Customer dataset


#state data
cust['state'] = cust['state'].apply(lambda x: 'New York' if x=='NY' else x)
cust['state'] = cust['state'].apply(lambda x: 'Texas' if x=='TX' else x)
cust['state'] = cust['state'].apply(lambda x: 'California' if x=='CALIFORNIA' else x)
cust['state'] = cust['state'].apply(lambda x: 'Massachusetts' if x=='MASS' else x)
cust['state'] = cust['state'].apply(lambda x: 'UNK' if x in ['Australia', '-999'] else x)

cust=cust[cust['state']!='UNK'].reset_index(drop=True)

#balance data - remove extremes and nulls
cust = cust[(cust['start_balance']>=0) & (cust['start_balance']<=1e6)].reset_index(drop=True)

#%%
# Transaction dataset

#ids dropped from cust
dropped_ids = [42704, 43658, 68359, 21334, 45982, 96030, 7012, 7968, 37710, 38895, 67790, 70699, 82548, 93096, 104611]
trans = trans[~trans['customer_id'].isin(dropped_ids)].reset_index(drop=True)

#extreme amounts
trans = trans[(trans['amount']>-1e9) & (trans['amount']<1e9)].reset_index(drop=True)

cust.to_csv('banking_churn_data/customers_clean.csv', index=False)
trans.to_csv('banking_churn_data/transactions_clean.csv', index=False)