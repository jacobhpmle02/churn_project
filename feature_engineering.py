'''
Aggregates cleaned customer and transaction data (obtained from data integrity)
and performs feature engineering for both internal and external data

(originally a notebook - converted to .py script for ease of use)
'''


#%%
# Imports

import pandas as pd
import numpy as np
from datetime import datetime, timedelta
print('Loading dataframes..')
cust = pd.read_csv('banking_churn_data/customers_clean.csv')
trans = pd.read_csv('banking_churn_data/transactions_clean.csv')


# %%
# Aggregation
print('Aggregating dataframes...')
trans['is_deposit'] = (trans['deposit']>0).astype(int)
trans['is_withdraw'] = (trans['withdrawal']<0).astype(int)
trans_agg = trans.groupby(['date', 'customer_id'], as_index=False).sum()
trans_agg = trans_agg.merge(cust, how='outer', on='customer_id')


# track balances through time
trans_agg['creation_month'] = (trans_agg['date']==trans_agg['creation_date']).astype(int)
trans_agg['month_start'] = trans_agg['start_balance'] * trans_agg['creation_month']
trans_agg['month_start'] = trans_agg['month_start'].apply(lambda x: np.nan if x==0 else x)
trans_agg['month_end'] = trans_agg['month_start'] +trans_agg['amount']

while trans_agg['month_end'].isnull().sum()>0:
    trans_agg['month_start']=trans_agg['month_start'].fillna(trans_agg.month_end.shift(1))
    trans_agg['month_end'] = trans_agg['month_start'] + trans_agg['amount']
    
# Define target

trans_agg['churn'] = (trans_agg['customer_id'].shift(-1)!=trans_agg['customer_id']).astype(int)

# %% 
# Internal FE - rolling features and age (Milan)
print('Rolling features...')
trans_agg['date_month'] = pd.to_datetime(trans_agg['date']).dt.to_period('m')
trans_agg['dob'] = pd.to_datetime(trans_agg['dob']).dt.to_period('m')
trans_agg['creation_date_month'] = pd.to_datetime(trans_agg['creation_date']).dt.to_period('m')
trans_agg['months_w_bank'] = (trans_agg['date_month'].astype(int) - trans_agg['creation_date_month'].astype(int))#
trans_agg['age'] = round((trans_agg['date_month'].astype(int) - trans_agg['dob'].astype(int))/12, 2)

new_agg = trans_agg.groupby('customer_id').amount.rolling(3).sum().reset_index().drop(['customer_id', 'level_1'], axis=1)
new_agg = new_agg.rename(columns={'amount':'amount_agg'})
trans_agg = pd.merge(trans_agg, new_agg, left_index=True, right_index=True)

# Rolling for is_deposit over 3 months
new_agg = trans_agg.groupby('customer_id').is_deposit.rolling(3).sum().reset_index().drop(['customer_id', 'level_1'], axis=1)
new_agg = new_agg.rename(columns={'is_deposit':'is_depo_roll'})
trans_agg = pd.merge(trans_agg, new_agg, left_index=True, right_index=True)

# Rolling for is_withdraw over 3 months
new_agg = trans_agg.groupby('customer_id').is_withdraw.rolling(3).sum().reset_index().drop(['customer_id', 'level_1'], axis=1)
new_agg = new_agg.rename(columns={'is_withdraw':'is_wdraw_roll'})
trans_agg = pd.merge(trans_agg, new_agg, left_index=True, right_index=True)

# %%
# State data and unemployment and GDP (Olivia)
print('Getting geographical data...')
state_data = pd.read_csv('https://raw.githubusercontent.com/cphalpert/census-regions/master/us%20census%20bureau%20regions%20and%20divisions.csv')
state_data = state_data.rename(columns={"State": "state"})
trans_agg = pd.merge(trans_agg,state_data, on='state', how='left')

trans_agg['date']=pd.to_datetime(trans_agg['date'])

def create_df(file_path, region):
    unemp = pd.read_excel(file_path)
    df = pd.DataFrame(unemp)
    df = df.iloc[10:]
    df = df.rename(columns={"Unnamed: 1": "unemployment_rate", "FRED Graph Observations": "date"})
    df['date']=df['date']-timedelta(days=1)
    df['Division']= f'{region}'
    return df

#Getting unemployment rate data for census regions
ENC_unemp_df = create_df('banking_churn_data/external/East North Central.xls', 'East North Central')
ESC_unemp_df = create_df('banking_churn_data/external/East South Central.xls', 'East South Central')
MAT_unemp_df = create_df('banking_churn_data/external/Middle Atlantic.xls', 'Middle Atlantic')
MOU_unemp_df = create_df('banking_churn_data/external/Mountain.xls', 'Mountain')
NEW_unemp_df = create_df('banking_churn_data/external/New England.xls', 'New England')
PAC_unemp_df = create_df('banking_churn_data/external/Pacific.xls', 'Pacific')
SAT_unemp_df = create_df('banking_churn_data/external/South Atlantic.xls', 'South Atlantic')
WNC_unemp_df = create_df('banking_churn_data/external/West North Central.xls', 'West North Central')
WSC_unemp_df = create_df('banking_churn_data/external/West South Central.xls', 'West South Central')

unemp_df = pd.concat([ENC_unemp_df,ESC_unemp_df,MAT_unemp_df, MOU_unemp_df,NEW_unemp_df,PAC_unemp_df,SAT_unemp_df,WNC_unemp_df,WSC_unemp_df])

#GDP data
GDP_df = pd.read_excel('banking_churn_data/external/GDP_quarterly.xls')
GDP_df = GDP_df.iloc[10:]
GDP_df = GDP_df.rename(columns={"Unnamed: 1": "GDP", "FRED Graph Observations": "date"})
GDP_df = GDP_df.rename(columns={"observed_date":"date"})
GDP_df['date']=GDP_df['date']-timedelta(days=1)

#merge GDP and uneployment rate data
GDP_unemp_df = pd.merge(unemp_df, GDP_df, on='date', how='left')
GDP_unemp_df = GDP_unemp_df.ffill() # as only reported quarterly

trans_agg = trans_agg.merge(GDP_unemp_df, on=['date', 'Division'], how='left')


# %% 
# Real Interest Rates (Oscar)
print('Getting real interest rates..')
inflation_file = r'banking_churn_data/external/Inflation_rate_monthly_US.csv'

df_inflation = pd.read_csv(inflation_file)

df_inflation.rename(columns={'DATE':'date', 'CPALTT01USM657N':'inflation_rate'}, inplace=True)
df_inflation['date'] = pd.to_datetime(df_inflation['date'], format='%d/%m/%Y')
df_inflation['date'] = df_inflation['date']-timedelta(days=1)
df_inflation.drop([0], axis=0, inplace=True)
df_inflation.drop(df_inflation.index[0], inplace=True)
df_inflation.drop(df_inflation.index[-1], inplace=True)
df_inflation.set_index(df_inflation['date'], inplace=True)
df_inflation['date'] = df_inflation['date'].dt.to_period('M')
df_inflation.rename(columns={'date':'date_month'}, inplace=True)

interest_rate_file = r'banking_churn_data/external/Interest_rates_monthly_US.csv'

df_interest_rate = pd.read_csv(interest_rate_file)

df_interest_rate.rename(columns={'DATE':'date', 'DFF':'interest_rate'}, inplace=True)
df_interest_rate['date'] = pd.to_datetime(df_interest_rate['date'], format='%d/%m/%Y')
df_interest_rate['date'] = df_interest_rate['date']-timedelta(days=1)
df_interest_rate.drop(df_interest_rate.index[0], inplace=True)
df_interest_rate.drop(df_interest_rate.index[-1], inplace=True)
df_interest_rate.set_index(df_interest_rate['date'], inplace=True)
df_interest_rate['date'] = df_interest_rate['date'].dt.to_period('M')
df_interest_rate.rename(columns={'date':'date_month'}, inplace=True)

df_external = df_interest_rate.copy()
df_external['inflation_rate'] = df_inflation['inflation_rate']
df_external['real_interest_rate'] = df_external['interest_rate'] - df_external['inflation_rate']
df_external.drop(columns=['interest_rate','inflation_rate'], axis=1, inplace=True)

trans_agg = trans_agg.merge(df_external, how='left', left_on=['date'], right_on=['date'])

# %% 
# Consumer sentiment (Ollie)
print('Getting consumer sentiment..')
sent = pd.read_csv('banking_churn_data/external/1UMCSENT.csv', index_col=0)
sent['DATE']= pd.to_datetime(sent['DATE'])
sent = sent[sent['DATE']>'2007-01-01']
sent['DATE'] = sent['DATE']-timedelta(days=1)
trans_agg = trans_agg.merge(sent, how='left', left_on=['date'], right_on=['DATE'])

# %%
# Interest rate and sentiment monthly change
delta_interest = pd.DataFrame(trans_agg.groupby('date')['real_interest_rate'].mean().diff().fillna(0))
delta_interest.columns = ['delta_interest']
delta_interest
trans_agg['UMCSENT'] = trans_agg['UMCSENT'].apply(float)
delta_sentiment = pd.DataFrame(trans_agg.groupby('date')['UMCSENT'].mean().diff().pct_change().fillna(0))
delta_sentiment.columns = ['delta_sentiment']
delta_sentiment
trans_agg = trans_agg.merge(delta_interest, on='date', how='left')
trans_agg = trans_agg.merge(delta_sentiment, on='date', how='left')

# %% 
# Splitting into train (01-2007 - 04-2020) and test (05-2020)

test =  trans_agg[trans_agg['date']=='2020-05-31'].reset_index()
train = trans_agg[trans_agg['date']!='2020-05-31'].reset_index()
print(test.shape)
print(train.shape)

# %%
# Filling in null values for rolling features (using mean)
print('Filling missing values...')
mean_amount = train['amount_agg'].mean()
mode_depo_roll = train['is_depo_roll'].mode()[0]
mode_wdraw_roll = train['is_wdraw_roll'].mode()[0]

train['amount_agg'] = train['amount_agg'].fillna(mean_amount)
train['is_depo_roll'] = train['is_depo_roll'].fillna(mode_depo_roll)
train['is_wdraw_roll'] = train['is_wdraw_roll'].fillna(mode_wdraw_roll)
test['amount_agg'] = test['amount_agg'].fillna(mean_amount)
test['is_depo_roll'] = test['is_depo_roll'].fillna(mode_depo_roll)
test['is_wdraw_roll'] = test['is_wdraw_roll'].fillna(mode_wdraw_roll)

# %%
# Drop temporary features
train.drop(['index','dob','state','creation_date','creation_month', 'date_month_x','creation_date_month','date_month_y','State Code','Region','DATE'], axis=1, inplace=True)
test.drop(['index','dob','state','creation_date','creation_month', 'date_month_x','creation_date_month','date_month_y','State Code','Region','DATE'], axis=1, inplace=True)

# %%
# Rename columns more intuitively

new_names = ['date','customer_id','month_bal_change','month_deposit', 'month_withdrawal',
 'num_deposit','num_withdrawal','opening_balance','month_start_bal', 'month_end_bal','churn','account_age',
'customer_age','3month_bal_change','3month_num_deposit','3month_num_withdraw','Region','Region_unemployment',
'GDP', 'real_interest_rate','consumer_sentiment', 'interest_change','sentiment_change']

train.columns = new_names
test.columns = new_names

#drop churn from test
test.drop('churn',axis=1, inplace=True)

# %%
# Save datasets
print('Saving datasets..')
train.to_csv('banking_churn_data/train_final.csv', index=False)
test.to_csv('banking_churn_data/test_final.csv', index=False)
print('..Done!')