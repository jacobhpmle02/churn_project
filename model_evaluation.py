# -*- coding: utf-8 -*-
"""
Evaluation of final model on validation and test
"""
#%% Imports 

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.metrics import plot_confusion_matrix, plot_precision_recall_curve, accuracy_score, roc_auc_score, precision_score, recall_score, f1_score, plot_roc_curve, precision_recall_curve, roc_curve, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.inspection import plot_partial_dependence
from lightgbm import plot_importance, LGBMClassifier
#import shap

VAL=False
# %% Sorting Features

train = pd.read_csv('banking_churn_data/train_final.csv')

train['date'] = pd.to_datetime(train['date'])

if VAL: # needs condensing
    train = train.set_index('date')
    #optional dropping
    train.drop(['num_deposit','month_start_bal','GDP'], axis=1, inplace=True)

    features = [col for col in train.columns if col not in ['customer_id','churn']]
    target='churn'
    X_train = train[features]
    X_train = pd.get_dummies(X_train)
    redundant_regions= ['Region_East North Central',
                        'Region_East South Central', 'Region_Middle Atlantic',
                        'Region_Mountain', 'Region_New England',
                        'Region_South Atlantic', 'Region_West North Central']
    X_train.drop(redundant_regions, axis=1, inplace=True)
    y_train = train[target]
    
    X_trn, X_val, y_trn, y_val = train_test_split(X_train, y_train, shuffle=False, test_size=0.1)
else:
    
    train['date']=pd.to_datetime(train['date'])

    val = train[train['date']>=pd.to_datetime('2020-03-31')]

    train = train[train['date']<pd.to_datetime('2020-03-31')]


    train = train.set_index('date')
    val = val.set_index('date')


    #optional dropping
    train.drop(['num_deposit','month_start_bal','GDP'], axis=1, inplace=True)
    val.drop(['num_deposit','month_start_bal','GDP'], axis=1, inplace=True)

    features = [col for col in train.columns if col not in ['customer_id','churn']]
    #print('Features:', features)
    target='churn'
    X_trn = train[features]
    X_trn = pd.get_dummies(X_trn)
    redundant_regions= ['Region_East North Central',
                        'Region_East South Central', 'Region_Middle Atlantic',
                        'Region_Mountain', 'Region_New England',
                        'Region_South Atlantic', 'Region_West North Central']
    X_trn.drop(redundant_regions, axis=1, inplace=True)
    y_trn = train[target]


    X_val = val[features]
    X_val = pd.get_dummies(X_val)
    redundant_regions= ['Region_East North Central',
                        'Region_East South Central', 'Region_Middle Atlantic',
                        'Region_Mountain', 'Region_New England',
                        'Region_South Atlantic', 'Region_West North Central']
    X_val.drop(redundant_regions, axis=1, inplace=True)
    y_val = val[target]


# %% Fit Model 

model= LGBMClassifier(subsample_for_bin=2000000,
                      n_estimators=244,
                      max_depth=11,
                      learning_rate=0.038657198003864156,
                      feature_fraction=1.0)

model.fit(X_trn,y_trn)

y_hat = model.predict_proba(X_val)[:,1]
y_pred = model.predict(X_val)
#y_pred = (model.predict_proba(X_val)[:,1]>0.4).astype(int)
print('AUC VAL', roc_auc_score(y_val, y_hat))
print('F1_val', f1_score(y_val, y_pred))


# %% roc p-r curve plot

fig, ax = plt.subplots(2,1, figsize=(10,10))
plot_roc_curve(model, X_val, y_val, ax=ax[0])
plot_precision_recall_curve(model,X_val,y_val, ax=ax[1])
ax[0].set_title('Receiver Operating Characteristic Curve - Probability of Customer Churn')
ax[1].set_title('Precision-Recall Curve - Probability of Customer Churn')
ax[0].legend(loc="lower right")
ax[1].legend(loc="upper right")


# %% confusion matrix

C  = confusion_matrix(y_val, y_pred)
print('True negatives:', C[0,0])
print('False Negatives:',C[1,0])
print('True Positives:', C[1,1])
print('False Positives:', C[0,1])


# %% Plot confusion matrix

plt.figure()
plot_confusion_matrix(model,X_val,y_val, cmap=plt.cm.Greens)
plt.title('Confusion matrix')
plt.show()
 
# %% Plot Importance

plt.figure(figsize=(20,20))
plot_importance(model)
plt.show()

# %%
# Partial dependence on validation

# stratified sampling for computational efficiency
X_val_churn = X_val[y_val==1]
X_val_nochurn = X_val[y_val==0]
X_val_churn = X_val_churn.sample(frac=0.5)
X_val_nochurn = X_val_nochurn.sample(frac=0.5)

 

X_val_pd= pd.concat([X_val_churn, X_val_nochurn])

for i in range(len(X_val.columns)):
    fig, ax = plt.subplots(figsize=(10,7), dpi=600)
    plot_partial_dependence(model, X_val_pd, [i], ax = ax)





# %% SHAP

#explainer = shap.TreeExplainer(y_pred, model)
#shap_values = explainer.shap_values(X_train)
#shap.summary_plot(shap_values, X_train)

